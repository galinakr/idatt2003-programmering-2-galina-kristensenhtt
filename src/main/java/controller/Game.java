package controller;

import models.DeckOfCards;
import models.Hand;
import models.PlayingCard;

import java.util.ArrayList;
import java.util.List;

/**
 * Controller class
 * Ensures smooth interaction between the UI and the models
 *
 * @version 1.0
 * @author Galina Kristensen
 */
public class Game {

  DeckOfCards deck;
  Hand hand;

  /**
   * Constructs a game object which instantiates deck and hand
   */
  public Game(){
    deck = new DeckOfCards();
    hand = new Hand();
  }

  /**
   * Method to deal out 5 cards from deck to hand
   */
  public void dealHand(){
    ArrayList<PlayingCard> deal = deck.dealHand(5);
    hand.addMultipleCards(deal);
  }

  /**
   * Method to check if there is a flush/straight, and which one
   * @return 0 if no flush
   *        1 if straight
   *        2 is flush
   *        3 is straight flush
   *        4 if royal flush
   */
  public int checkHand(){
    if(hand.isRoyalFlush()) {
      return 4;
    }

    if(hand.isStraightFlush()) {
      return 3;
    }

    if(hand.isStraight()) {
      return 1;
    }

    if(hand.isFlush()) {
      return 2;
    }

    return 0;
  }

  /**
   * Sums up the total of faces
   * e.g: H5 + C3 = 8 "faces"
   *
   * @return the sum
   */
  public int sum(){
    return hand.toList().stream().mapToInt(PlayingCard::getFace).sum();
  }

  /**
   * Filter out the hearts of the hand,
   * and return as a string
   *
   * @return String of the list
   */
  public String getHearts(){
    List<PlayingCard> hearts = hand.toList().stream().filter(p -> p.getSuit() == 'H').toList();

    StringBuilder s = new StringBuilder();

    hearts.forEach(playingCard -> {
      s.append(playingCard.getAsString()).append(" ");
    });

    return s.toString();
  }

  /**
   * check if there is a spade of queen in hand
   * create a playing card S12 and return whether the list contains  it
   *
   * @return true if S12 is found
   *         false if not found
   */
  public boolean spadesQueen(){
    PlayingCard target = new PlayingCard('S',12);

    return hand.toList().contains(target);
  }

  /**
   * Checks if the hand has a flush
   * 5 of the same suit
   *
   * @return true if flush
   *         false if no flush
   */
  public boolean flush(){
    return hand.isFlush();
  }

  /**
   * Create a string out of the cards in hand
   *
   * @return return string
   */
  public String getCards(){
    StringBuilder s = new StringBuilder();

    hand.toList().forEach(playingCard -> {
      s.append(playingCard.getAsString()).append(" ");
    });

    return s.toString();
  }

  /**
   * return all cards back to deck and remove all cards from hand
   */
  public void reset(){
    deck.reset();
    hand.reset();
  }
}
