package models;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;

/**
 * Create a hand
 *
 * @version 1.1
 * @author Galina Kristensen
 */

public class Hand {
  private ArrayList<PlayingCard> hand;

  /**
   * Construct an empty hand
   */
  public Hand(){
    hand = new ArrayList<PlayingCard>();
  }

  /**
   * Add card to hand
   *
   * @param c the added card
   */
  public void addCard(PlayingCard c){
    if(!hand.contains(c)){
      hand.add(c);
    }
  }

  /**
   * Add multiple card to hand
   *
   * @param list of cards
   */
  public void addMultipleCards(ArrayList<PlayingCard> list){
    list.forEach(this::addCard);
  }

  /**
   * Get card based on suit and face as a String
   *
   * @param card the String
   * @return the models.PlayingCard if found, null if not found
   */
  public PlayingCard getCard(String card) {
    for (PlayingCard c : hand) {
      String s = String.format("%s%s", c.getSuit(), c.getFace());
      if (s.equalsIgnoreCase(card)) {
        return c;
      }
    }
    return null;
  }

  /**
   * Get card based on its position in the sorted deck
   * Throw new illegal argument exception if index is out of bounds of deck
   *
   * @param index the position
   * @return card in given index
   */
  public PlayingCard getCard(int index) {
    if(index < 0 || index >= hand.size()){
      throw new IllegalArgumentException("Index must be within the bounds of the hand.");
    }

    return hand.get(index);
  }

  /**
   * Remove a card from the hand
   *
   * @param c card to be removed
   */
  public void removeCard(PlayingCard c){
    hand.remove(c);
  }

  /**
   * remove all cards from hand
   */
  public void reset(){
    hand.clear();
  }

  /**
   * Sort the hand by suit
   */
  public void sortBySuit() {
    hand.sort(Comparator.comparing(PlayingCard::getSuit));
  }

  /**
   * Sort the hand by face
   */
  public void sortByFace() {
    hand.sort(Comparator.comparing(PlayingCard::getFace));
  }

  /**
   * Checks if hand contains a flush
   * Sorts the hand
   *
   * @return True if first and last card in hand are the same suit
   *          False if they are different
   */
  public boolean isFlush() {
    if(hand.isEmpty()){
      return false;
    }

    sortBySuit();
    return hand.get(0).getSuit() == hand.get(4).getSuit();
  }

  /**
   * Checks if the hand contains a straight
   * Sorts hand in ascending order
   *
   * @return True if iterator check is completed
   *          False if cards the next card in iterator isn't larger by 1
   */
  public boolean isStraight() {
    if(hand.isEmpty()){
      return false;
    }

    sortByFace();
    Iterator<PlayingCard> it = hand.iterator();

    int lowestFace = hand.getFirst().getFace();

    while(it.hasNext()){
      if(lowestFace != it.next().getFace() - 1){
        return false;
      }
      lowestFace = it.next().getFace();
    }

    return true;
  }

  /**
   * Checks if hand contains a straight flush
   *
   * @return True if hand contains both a flush and a straight
   *          False if not
   */
  public boolean isStraightFlush() {
    return isFlush() && isStraight();
  }

  /**
   * Checks if hand contains a royal flush
   *
   * @return True if hand contains a straight flush and the last card is ace
   *          False if not
   */
  public boolean isRoyalFlush() {
    sortByFace();
    return isStraightFlush() && hand.getLast().getFace() == 13;
  }

  /**
   * Return hand as an ArrayList
   * @return ArrayList of hand
   */
  public ArrayList<PlayingCard> toList() {
    return hand;
  }

  /**
   * toString hand
   *
   * @return String of the cards in the hand
   */
  @Override
  public String toString(){
    StringBuilder s = new StringBuilder();

    for(PlayingCard c : hand){
      s.append(c.getAsString()).append(" ");
    }

    return s.toString();
  }

}
