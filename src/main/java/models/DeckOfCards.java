package models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

/**
 * Creates a deck of 52 cards with models.PlayingCard class
 *
 * @version 1.3
 * @author Galina Kristensen
 */

public class DeckOfCards {
  private final char[] suit = {'S', 'H', 'D', 'C'};
  private int[] availableCards;
  private ArrayList<PlayingCard> deck = new ArrayList<>();

  /**
   * Construct a deck with 52 cards, 13 cards per suit
   */
  public DeckOfCards() {
    for (char c : suit) {
      for (int j = 0; j < 13; j++) {
        deck.add(new PlayingCard(c, j + 1));
      }
    }
    availableCards = new int[52];
    Arrays.fill(availableCards, 1);
  }

  /**
   * Get card based on its position in the sorted deck
   * Throw new illegal argument exception if index is out of bounds of deck
   *
   * @param index the position
   * @return card in given index
   */
  public PlayingCard getCard(int index) {
    if(index < 0 || index >= 52){
      throw new IllegalArgumentException("Index must be within the bounds of the deck.");
    }

    return deck.get(index);
  }

  /**
   * Get card based on suit and face as a String
   *
   * @param card the String
   * @return the models.PlayingCard if found, null if not found
   */
  public PlayingCard getCard(String card) {
    for (PlayingCard c : deck) {
      String s = String.format("%s%s", c.getSuit(), c.getFace());
      if (s.equalsIgnoreCase(card)) {
        return c;
      }
    }
    return null;
  }

  /**
   * Make deck into String
   * For testing purpose
   *
   * @return the deck as a String
   */
  public String deckToString() {
    StringBuilder s = new StringBuilder();
    int i = 0;

    for(PlayingCard c : deck){
      if(availableCards[i] == 1){
       s.append(c.getAsString()).append(" ");
      }
      if(i == 12 || i == 25 || i == 38 || i == 51) {
        s.append("\n");
      }
      i++;
    }

    return s.toString();
  }

  /**
   * Make a card in deck unavailable
   *
   * @param i the position of the card in a sorted deck
   */
  public void removeFromDeck(int i) {
    availableCards[i] = 0;
  }

  /**
   * Return all cards to deck
   * make all cards available
   */
  public void reset(){
    Arrays.fill(availableCards,1);
  }

  /**
   * Make a card in deck available
   *
   * @param i the position of the card in a sorted deck
   */
  public void addToDeck(int i) {
    availableCards[i] = 1;
  }

  /**
   * Pick n random card(s)
   *
   * @param n amount of cards
   * @return ArrayList with the chosen cards
   */
  public ArrayList<PlayingCard> dealHand(int n){
    if(n < 1 || n > 52){
      throw new IllegalArgumentException("The number of dealt cards must be within the bounds of the deck.");
    }

    ArrayList<PlayingCard> deal = new ArrayList<>(n);

    if(n < 52) {
      Random r = new Random();

      while(deal.size() < n) {
        int[] list = r.ints(1, 52)
                .distinct()
                .limit(n)
                .toArray();

        for (int i : list) {
          deal.add(getCard(i - 1));
          removeFromDeck(i - 1);
        }
        deal.stream().distinct();
      }
    }else{
      deal.addAll(deck);
      Arrays.fill(availableCards, 0);
    }

    return deal;
  }
}
