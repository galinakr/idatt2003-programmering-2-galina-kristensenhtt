package view;

import controller.Game;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class MainWindow extends Application {
  Game game = new Game();
  Text cards;
  TextField sumFacesTF = new TextField();
  TextField cardsOfHeartsTF = new TextField();
  TextField flushTF = new TextField();
  TextField queenOfSpadesTF = new TextField();


  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) throws Exception {
    //set up main window
    primaryStage.setTitle("Card Game");
    primaryStage.setResizable(false);

    StackPane root = new StackPane();

    //GridPane for layout
    GridPane grid = new GridPane();
    grid.setVgap(10);
    grid.setHgap(10);
    grid.setPadding(new Insets(10,10,10,10));

    //Field for displaying cards
    VBox gameField = new VBox();
    gameField.setStyle("-fx-padding: 20px; -fx-background-color: green");
    Text cards = new Text();
    cards.setFont(new Font("Arial",36));
    gameField.getChildren().add(cards);

    //Buttons to deal or check hand
    Button dealHand = new Button("Deal hand");
    dealHand.setOnAction((event) -> {
      game.reset();
      game.dealHand();
      cards.setText(game.getCards());
    });

    //Text fields for oppg5
    //Sum faces
    Text sumFacesT = new Text("Sum of faces: ");
    TextField sumFacesTF = new TextField();
    sumFacesTF.setPromptText("Sum of faces");
    sumFacesTF.setText(String.valueOf(game.sum()));

    sumFacesTF.setOnAction((event) -> {
      sumFacesTF.setText(String.valueOf(game.sum()));
    });

    HBox textCollect1 = new HBox();
    textCollect1.getChildren().addAll(sumFacesT,sumFacesTF);

    //Cards of hearts text field
    Text cardsOfHeartsT = new Text("Cards of hearts: ");
    TextField cardsOfHeartsTF = new TextField();
    cardsOfHeartsTF.setPromptText("Cards of hearts");
    cardsOfHeartsTF.setText(game.getHearts());

    cardsOfHeartsTF.setOnAction((event) -> {
      cardsOfHeartsTF.setText(game.getHearts());
    });

    HBox textCollect2 = new HBox();
    textCollect2.getChildren().addAll(cardsOfHeartsT,cardsOfHeartsTF);

    //Flush text field
    Text flushT = new Text("Flush? ");
    TextField flushTF = new TextField();
    flushTF.setPromptText("Y/N");

    if(game.flush()){
      flushTF.setText("Yes");
    }else{
      flushTF.setText("No");
    }

    flushTF.setOnAction((event) -> {
      if(game.flush()){
        flushTF.setText("Yes");
      }else{
        flushTF.setText("No");
      }
    });

    HBox textCollect3 = new HBox();
    textCollect3.getChildren().addAll(flushT,flushTF);

    //Queen of spades
    Text queenOfSpadesT = new Text("Queen of spades? ");
    TextField queenOfSpadesTF = new TextField();
    queenOfSpadesTF.setPromptText("Y/N");

    if(game.spadesQueen()){
      queenOfSpadesTF.setText("Yes");
    }else{
      queenOfSpadesTF.setText("No");
    }

    queenOfSpadesTF.setOnAction((event) -> {
      if(game.spadesQueen()){
        queenOfSpadesTF.setText("Yes");
      }else{
        queenOfSpadesTF.setText("No");
      }
    });

    HBox textCollect4 = new HBox();
    textCollect4.getChildren().addAll(queenOfSpadesT,queenOfSpadesTF);

    //Add components to window
    VBox bottomField = new VBox();
    bottomField.setPadding(new Insets(15,15,15,15));
    bottomField.getChildren().addAll(textCollect1,textCollect2,textCollect3,textCollect4);


    Button checkHand = new Button("Check hand");
    checkHand.setOnAction((event) -> {
      if(game.spadesQueen()){
        queenOfSpadesTF.setText("Yes");
      }else{
        queenOfSpadesTF.setText("No");
      }

      if(game.flush()){
        flushTF.setText("Yes");
      }else{
        flushTF.setText("No");
      }

      cardsOfHeartsTF.setText(game.getHearts());

      sumFacesTF.setText(String.valueOf(game.sum()));
    });

    grid.add(gameField, 0,0,40,40);
    grid.add(dealHand,41,19,2,1);
    grid.add(checkHand,41,20,2,1);
    grid.add(bottomField,0,41,5,5);

    //display
    root.getChildren().add(grid);
    primaryStage.setScene(new Scene(root, 800, 600));
    primaryStage.show();
  }

}
