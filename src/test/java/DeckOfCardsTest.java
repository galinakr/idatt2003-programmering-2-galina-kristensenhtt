import models.DeckOfCards;
import models.PlayingCard;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test class for models.DeckOfCards class
 * First parameterized to test accessor methods
 * Then a visual String test to ensure the class is constructed properly
 *
 * @version 1.0
 * @author Galina Kristensen
 */


public class DeckOfCardsTest {
  @ParameterizedTest
  @CsvSource({
          "0, s10, S,1, S,10",
          "5, h1, S,6, H,1",
          "51, d13, C,13, D,13",
          "48, c1, C,10, C,1",
          "15, S8, H,3, S,8"
  })

  @DisplayName("Test accessor-methods")
  public void testAccessor(
          int index,
          String card,
          char e1Suit,
          int e1Face,
          char e2Suit,
          int e2Face
  ) {
    DeckOfCards deck = new DeckOfCards();

    PlayingCard e1 = new PlayingCard(e1Suit,e1Face);
    PlayingCard e2 = new PlayingCard(e2Suit,e2Face);

    PlayingCard r1 = deck.getCard(index);
    PlayingCard r2 = deck.getCard(card);

    //positive
    try{
      assertEquals(e1, r1);
      assertEquals(e2, r2);
    } catch (Exception e) {
      System.out.println("Unexpected exception: " + e.getMessage());
    }

    //negative
    try{
      assertNotEquals(e2, deck.getCard(index));
      assertNotEquals(e1, deck.getCard(card));
    } catch (Exception e) {
      System.out.println("Unexpected exception: " + e.getMessage());
    }
  }

  @Test
  @DisplayName("Visual test")
  public void visualTest() {
    DeckOfCards deck = new DeckOfCards();
    System.out.println(deck.deckToString());

  }

  @ParameterizedTest
  @CsvSource({
          "1",
          "2",
          "5",
          "30",
          "52"
  })
  @DisplayName("dealHand test")
  public void dealHandTest(
          int n
  ) {
    DeckOfCards deck = new DeckOfCards();

    ArrayList<PlayingCard> hand = deck.dealHand(n);

    System.out.println("Cards in hand:");
    StringBuilder s = new StringBuilder();
    int i = 0;
    for(PlayingCard c : hand){
      s.append(c.getAsString()).append(" ");
      if(i == 12 || i == 25 || i == 38 || i == 51) {
        s.append("\n");
      }
      i++;
    }
    System.out.println(s.toString());

    System.out.println("\nCards in deck: \n" + deck.deckToString());

  }
}